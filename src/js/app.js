require('../css/app.css');
import isValidPhoneNumber from '../../node_modules/libphonenumber-js/index';
import L from 'leaflet';
import _ from 'lodash';
import Chart from 'chart.js/auto';
import dayjs from 'dayjs';

function getStatsByAge(users) {
    let data = [];
    data.push(_.filter(users, (user) => { return user.age <= 31; }).length);
    data.push(_.filter(users, (user) => { return user.age > 31 && user.age <= 45; }).length);
    data.push(_.filter(users, (user) => { return user.age > 45 && user.age <= 59; }).length);
    data.push(_.filter(users, (user) => { return user.age > 60; }).length);

    let output = {
        labels: ["18-31", "32-45", "46-59", "60+"],
        datasets: [
            {
                backgroundColor: [
                    'rgb(255, 99, 132)',
                    'rgb(54, 162, 235)',
                    'rgb(255, 205, 86)',
                    'rgb(175, 255, 86)'
                ],
                label: 'Count',
                data: data
            }
        ]
    }
    return output;
}
function getStatsByGender(users) {
    let data = [];
    data.push(_.filter(users, (user) => { return user.gender === "male"; }).length);
    data.push(users.length - data[0]);

    let output = {
        labels: ["Male", "Female"],
        datasets: [
            {
                backgroundColor: [
                    'rgb(54, 162, 235)',
                    'rgb(255, 99, 132)'
                ],
                label: 'Count',
                data: data
            }
        ]
    }
    return output;
}
function getStatsByCountry(users) {
    let u = _.groupBy(users, 'country');
    let data = [];
    for (let i in u) {
        data.push({
            name: i,
            count: u[i].length
        });
    }
    data = getSorted(data, 'count', true)

    let color = [];
    for (let i = 0; i < data.length; i++) {
        color.push('#' + _.random(10000, 16000000).toString(16));
    }

    let output = {
        labels: _.map(data, 'name'),
        datasets: [
            {
                backgroundColor: color,
                label: 'Count',
                data: _.map(data, 'count')
            }
        ]
    }
    return output;
}
function getStatsByCourse(users) {
    let u = _.groupBy(users, 'course');
    let data = [];
    for (let i in u) {
        data.push({
            name: i,
            count: u[i].length
        });
    }
    data = getSorted(data, 'count', true)

    let color = [];
    for (let i = 0; i < data.length; i++) {
        color.push('#' + _.random(10000, 16000000).toString(16));
    }

    let output = {
        labels: _.map(data, 'name'),
        datasets: [
            {
                backgroundColor: color,
                label: 'Count',
                data: _.map(data, 'count')
            }
        ]
    }
    return output;
}
function getStatsByFavorite(users) {
    let data = [];
    data.push(_.filter(users, (user) => { return user.favorite }).length);
    data.push(users.length - data[0]);
    let output = {
        labels: ["Favorite", "Not favorite"],
        datasets: [
            {
                backgroundColor: [
                    'rgb(255, 205, 86)',
                    'rgb(175, 255, 86)'
                ],
                label: 'Count',
                data: data
            }
        ]
    }
    return output;
}

function StatsBtn(btn, func) {
    if (!btn.className.includes("selected")) {
        let ul = document.getElementById("stats-button").children;
        for (let b of ul) {
            b.className = "button";
        }
        btn.className += " selected";
        statFunc = func;
        updateStats(statFunc(users_all));
    }
}
function btnInit(funcs) {
    let ul = document.getElementById("stats-button").children;
    ul[0].className += " selected";

    for (let i = 0; i < ul.length; i++) {
        ul[i].firstChild.addEventListener('click', (event) => { StatsBtn(ul[i], funcs[i]); })
    }
}

function getDaysCount(user) {
    let dayOfYear = require('dayjs/plugin/dayOfYear');
    dayjs.extend(dayOfYear);
    return (dayjs(user.b_date).dayOfYear() - dayjs(dayjs()).dayOfYear() + 365) % 365;
}
function getSorted(array, column_name = "", DowntoUp = false) {
    let narray = _.sortBy(array, [(a) => { return a[column_name] }]);

    return DowntoUp ? _.reverse(narray) : narray;
}
function validateUser(user) {

    if (user.full_name === "") {
        alert("Name isn't correct");
        return false;
    } else if (user.full_name !== _.startCase(user.full_name)) {
        alert("Name isn't correct");
        return false;
    }

    if (user.course === "") {
        alert("Speciality isn't correct");
        return false;
    }

    if (user.country === "") {
        alert("Country isn't correct");
        return false;
    }

    if (user.gender === "") {
        alert("Gender isn't correct");
        return false;
    }

    if (user.city === "") {
        alert("City isn't correct");
        return false;
    } else if (user.city !== _.upperFirst(user.city)) {
        alert("City isn't correct");
        return false;
    }

    if (!user.email.includes("@")) {
        alert("Email isn't correct");
        return false;
    }

    if (!isValidPhoneNumber(user.phone, countries.find(countries => countries.name === user.country).code)) {
        alert("Phone number isn't correct");
        return false;
    }

    if (Number.isNaN(user.age) || user.age < 18) {
        alert("Age isn't correct");
        return false;
    }

    if (user.gender == undefined) {
        alert("Gender isn't correct");
        return false;
    }


    alert(user.full_name + " is added");
    return true;
}
function getNewUsers(userMock) {
    let courses = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", "Chess", "Biology", "Chemistry", "Law", "Art", "Medicine", "Statistics"];
    const users = [];

    for (let i = 0; i < userMock.length; i++) {
        let user = userMock[i];

        let formattedUser = {
            "gender": user.gender,
            "title": user.name.title,
            "full_name": user.name.first + " " + user.name.last,
            "city": user.location.city,
            "country": user.location.country,
            "postcode": user.location.postcode,
            "coordinates": user.location.coordinates,
            "timezone": user.location.timezone,
            "email": user.email,
            "b_date": user.dob.date,
            "age": user.dob.age,
            "phone": user.phone,
            "picture_large": user.picture.large,
            "picture_thumbnail": user.picture.thumbnail,
            "id": user.id.name + user.id.value,
            "favorite": !!_.random(2),
            "course": courses[_.random(courses.length - 1)],
            "bg_color": "#" + _.random(16777215).toString(16),
            "note": ""
        }

        users.push(formattedUser);
    }

    return users;
}
function findUsers(users, search) {

    return _.filter(users, (user) => { return user.full_name.toLowerCase().includes(search.toLowerCase()) || user.note.toLowerCase().includes(search.toLowerCase()) || user.age == search; })

}
function getFiltredUsers(users, country, age, gender, favorite) {

    let nusers = _.filter(users, (user) => { return user.country.toLowerCase().includes(country.toLowerCase()) && age(user) && user.gender.startsWith(gender); });
    return favorite ? _.filter(nusers, (user) => { return user.favorite; }) : nusers;
}

async function getUsers(count = 50) {
    users_all = users_all.concat(await importUsers(count));
    renderUsers(true);
}
function createUserElement(user) {
    let figure = document.createElement('figure');
    figure.className = "favorite-img";

    let frame = document.createElement('div');
    frame.className = "main-image-conteiner";

    frame.append(getImageCover(user));
    figure.append(frame);

    let name = document.createElement('p');
    name.className = "name";
    name.textContent = user.full_name;
    figure.append(name);
    let region = document.createElement('p');
    region.className = "region";
    region.textContent = user.country;
    figure.append(region);
    return figure;
}
function getImageCover(user) {
    let image = document.createElement('img');
    image.className = "mainimage";
    image.onclick = function () {
        let info = document.querySelectorAll(".info");
        document.getElementById('teacher-view').style.visibility = 'visible';
        info[0].src = user.picture_large;
        info[1].src = user.favorite ? "images/Star-filled.png" : "images/Star.png";
        info[1].onclick = function () {
            user.favorite = !user.favorite;
            this.src = user.favorite ? "images/Star-filled.png" : "images/Star.png";
            updateFavorites(getFiltredUsers(users_all, "", function (user) { return true; }, "", true));
            renderUsers(true);
        };
        info[2].textContent = user.full_name;
        info[3].textContent = user.course;
        info[4].textContent = user.city + " , " + user.country;
        info[5].textContent = user.age + ", " + user.gender;
        info[6].textContent = user.email;
        info[6].href = "mailto:" + user.email;
        info[7].textContent = user.phone;
        let d = getDaysCount(user);
        info[8].textContent = d === 0 ? "Birthday: today!" : `Birthday: ${d} days left`;

        info[9].textContent = user.note;
        if (user.coordinates.latitude != undefined && user.coordinates.longitude != undefined) {
            document.getElementById('map').outerHTML = '<div id="map"></div>';
            let map = L.map('map').setView([parseFloat(user.coordinates.latitude), parseFloat(user.coordinates.longitude)], 10);
            L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 15,
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }).addTo(map);
            L.marker([parseFloat(user.coordinates.latitude), parseFloat(user.coordinates.longitude)]).addTo(map);
        }
    };
    image.src = user.picture_large;
    let text = "";
    for (let t of user.full_name.split(" ")) {
        text += t[0] + ".";
    }
    image.alt = text;
    return image;
}
function updateContent(users) {
    let imageList = document.getElementById("imagelist");
    imageList.innerHTML = "";
    let fragmet = document.createDocumentFragment();
    for (let i = 0; i < users.length; i++) {
        let user = users[i];
        let figure = document.createElement('figure');
        let star = document.createElement('img');
        star.className = "favor-sign";
        star.src = "images/Star-filled.png";
        star.alt = "Star";
        figure.append(star);

        let frame = document.createElement('div');
        frame.className = "main-image-conteiner";

        frame.append(getImageCover(user));
        figure.append(frame);
        let name = document.createElement('p');
        name.className = "name";
        name.textContent = user.full_name;
        figure.append(name);
        let subject = document.createElement('p');
        subject.className = "subject";
        subject.textContent = user.course;
        figure.append(subject);
        let region = document.createElement('p');
        region.className = "region";
        region.textContent = user.country;
        figure.append(region);
        fragmet.append(figure);
    }
    imageList.append(fragmet);
}
function updateFavorites(users, currentIndex = 0) {
    let imageList = document.getElementById("imagelist-favorites");
    imageList.innerHTML = "";
    let fragmet = document.createDocumentFragment();
    if (users.length <= 5) {
        let div = document.createElement("div");
        div.className = "imagelist";
        div.style.margin = "0 100px";

        for (let i = 0; i < users.length; i++) {
            div.append(createUserElement(users[i]));
        }
        fragmet.append(div);
        imageList.append(fragmet);
        return;
    }

    let count = Math.ceil(users.length / 5);
    let arrow = document.createElement("img");
    arrow.className = "sign";
    arrow.onclick = function () {
        updateFavorites(users, (currentIndex - 1 + count) % count);
    }
    fragmet.append(arrow);

    let div = document.createElement("div");
    div.className = "imagelist";

    for (let i = 0; i < 5; i++) {
        div.append(createUserElement(users[(currentIndex * 5 + i) % users.length]));
    }
    fragmet.append(div);

    arrow = document.createElement("img");
    arrow.className = "sign";
    arrow.onclick = function () {
        updateFavorites(users, (currentIndex + 1) % count);
    }
    fragmet.append(arrow);
    imageList.append(fragmet);
}
function filterUsers() {
    let selects = document.querySelectorAll(".filter-input");
    let search = document.getElementById("search");

    let func;
    switch (selects[0].value.toString()) {
        case "1":
            func = function (user) { return user.age <= 31; }
            break;
        case "2":
            func = function (user) { return user.age > 31 && user.age <= 45; }
            break;
        case "3":
            func = function (user) { return user.age > 45 && user.age <= 59; }
            break;
        case "4":
            func = function (user) { return user.age > 60; }
            break;
        default:
            func = function (user) { return true; }
            break;
    }
    let fusers = getFiltredUsers(users_all, selects[1].value, func, selects[2].value, selects[4].checked);
    return findUsers(fusers, search.value);

}
function updateStats(data) {
    statistic.destroy();
    statistic = new Chart(document.getElementById("stats"),
        {
            type: 'pie',
            options: {
                plugins: {
                    legend: {
                        display: true
                    },
                    tooltip: {
                        enabled: true
                    }
                }
            },
            data: data
        });

}
function renderUsers(renderFavorites = false) {
    updateContent(filterUsers());

    updateStats(statFunc(users_all));
    if (renderFavorites) {
        updateFavorites(getFiltredUsers(users_all, "", function (user) { return true; }, "", true));
    }
}
function addUser() {
    let inputs = document.querySelectorAll(".input");
    let gend = document.getElementsByName("gender");
    let note = document.querySelector("textarea");

    let user = {
        "id": _.uniqueId(),
        "gender": gend[0].checked ? gend[0].value : gend[1].checked ? gend[1].value : null,
        "title": "",
        "full_name": inputs[0].value,
        "city": inputs[3].value,
        "country": inputs[2].value,
        "postcode": "",
        "coordinates": {},
        "timezone": {},
        "email": inputs[4].value,
        "b_date": inputs[6].value,
        "age": _.floor((dayjs() - dayjs(inputs[6].value)) / 31536000000),
        "phone": inputs[5].value,
        "picture_large": "",
        "picture_thumbnail": "",
        "favorite": false,
        "course": inputs[1].value,
        "bg_color": inputs[7].value,
        "note": note.value
    }

    if (validateUser(user, users_all)) {
        addUsers(user);
        users_all.push(user);
        renderUsers();
        document.getElementById("form").reset();
        document.getElementById('teacher-add-menu').style.visibility = 'hidden';
    }

}

const countries = [
    { "name": "Afghanistan", "code": "AF" },
    { "name": "Åland Islands", "code": "AX" },
    { "name": "Albania", "code": "AL" },
    { "name": "Algeria", "code": "DZ" },
    { "name": "American Samoa", "code": "AS" },
    { "name": "Andorra", "code": "AD" },
    { "name": "Angola", "code": "AO" },
    { "name": "Anguilla", "code": "AI" },
    { "name": "Antarctica", "code": "AQ" },
    { "name": "Antigua and Barbuda", "code": "AG" },
    { "name": "Argentina", "code": "AR" },
    { "name": "Armenia", "code": "AM" },
    { "name": "Aruba", "code": "AW" },
    { "name": "Australia", "code": "AU" },
    { "name": "Austria", "code": "AT" },
    { "name": "Azerbaijan", "code": "AZ" },
    { "name": "Bahamas", "code": "BS" },
    { "name": "Bahrain", "code": "BH" },
    { "name": "Bangladesh", "code": "BD" },
    { "name": "Barbados", "code": "BB" },
    { "name": "Belarus", "code": "BY" },
    { "name": "Belgium", "code": "BE" },
    { "name": "Belize", "code": "BZ" },
    { "name": "Benin", "code": "BJ" },
    { "name": "Bermuda", "code": "BM" },
    { "name": "Bhutan", "code": "BT" },
    { "name": "Bolivia", "code": "BO" },
    { "name": "Bosnia and Herzegovina", "code": "BA" },
    { "name": "Botswana", "code": "BW" },
    { "name": "Bouvet Island", "code": "BV" },
    { "name": "Brazil", "code": "BR" },
    { "name": "British Indian Ocean Territory", "code": "IO" },
    { "name": "Brunei Darussalam", "code": "BN" },
    { "name": "Bulgaria", "code": "BG" },
    { "name": "Burkina Faso", "code": "BF" },
    { "name": "Burundi", "code": "BI" },
    { "name": "Cambodia", "code": "KH" },
    { "name": "Cameroon", "code": "CM" },
    { "name": "Canada", "code": "CA" },
    { "name": "Cape Verde", "code": "CV" },
    { "name": "Cayman Islands", "code": "KY" },
    { "name": "Central African Republic", "code": "CF" },
    { "name": "Chad", "code": "TD" },
    { "name": "Chile", "code": "CL" },
    { "name": "China", "code": "CN" },
    { "name": "Christmas Island", "code": "CX" },
    { "name": "Cocos (Keeling) Islands", "code": "CC" },
    { "name": "Colombia", "code": "CO" },
    { "name": "Comoros", "code": "KM" },
    { "name": "Congo", "code": "CG" },
    { "name": "Congo, The Democratic Republic of the", "code": "CD" },
    { "name": "Cook Islands", "code": "CK" },
    { "name": "Costa Rica", "code": "CR" },
    { "name": "Cote D\"Ivoire", "code": "CI" },
    { "name": "Croatia", "code": "HR" },
    { "name": "Cuba", "code": "CU" },
    { "name": "Cyprus", "code": "CY" },
    { "name": "Czech Republic", "code": "CZ" },
    { "name": "Denmark", "code": "DK" },
    { "name": "Djibouti", "code": "DJ" },
    { "name": "Dominica", "code": "DM" },
    { "name": "Dominican Republic", "code": "DO" },
    { "name": "Ecuador", "code": "EC" },
    { "name": "Egypt", "code": "EG" },
    { "name": "El Salvador", "code": "SV" },
    { "name": "Equatorial Guinea", "code": "GQ" },
    { "name": "Eritrea", "code": "ER" },
    { "name": "Estonia", "code": "EE" },
    { "name": "Ethiopia", "code": "ET" },
    { "name": "Falkland Islands (Malvinas)", "code": "FK" },
    { "name": "Faroe Islands", "code": "FO" },
    { "name": "Fiji", "code": "FJ" },
    { "name": "Finland", "code": "FI" },
    { "name": "France", "code": "FR" },
    { "name": "French Guiana", "code": "GF" },
    { "name": "French Polynesia", "code": "PF" },
    { "name": "French Southern Territories", "code": "TF" },
    { "name": "Gabon", "code": "GA" },
    { "name": "Gambia", "code": "GM" },
    { "name": "Georgia", "code": "GE" },
    { "name": "Germany", "code": "DE" },
    { "name": "Ghana", "code": "GH" },
    { "name": "Gibraltar", "code": "GI" },
    { "name": "Greece", "code": "GR" },
    { "name": "Greenland", "code": "GL" },
    { "name": "Grenada", "code": "GD" },
    { "name": "Guadeloupe", "code": "GP" },
    { "name": "Guam", "code": "GU" },
    { "name": "Guatemala", "code": "GT" },
    { "name": "Guernsey", "code": "GG" },
    { "name": "Guinea", "code": "GN" },
    { "name": "Guinea-Bissau", "code": "GW" },
    { "name": "Guyana", "code": "GY" },
    { "name": "Haiti", "code": "HT" },
    { "name": "Heard Island and Mcdonald Islands", "code": "HM" },
    { "name": "Holy See (Vatican City State)", "code": "VA" },
    { "name": "Honduras", "code": "HN" },
    { "name": "Hong Kong", "code": "HK" },
    { "name": "Hungary", "code": "HU" },
    { "name": "Iceland", "code": "IS" },
    { "name": "India", "code": "IN" },
    { "name": "Indonesia", "code": "ID" },
    { "name": "Iran, Islamic Republic Of", "code": "IR" },
    { "name": "Iraq", "code": "IQ" },
    { "name": "Ireland", "code": "IE" },
    { "name": "Isle of Man", "code": "IM" },
    { "name": "Israel", "code": "IL" },
    { "name": "Italy", "code": "IT" },
    { "name": "Jamaica", "code": "JM" },
    { "name": "Japan", "code": "JP" },
    { "name": "Jersey", "code": "JE" },
    { "name": "Jordan", "code": "JO" },
    { "name": "Kazakhstan", "code": "KZ" },
    { "name": "Kenya", "code": "KE" },
    { "name": "Kiribati", "code": "KI" },
    { "name": "Korea, Democratic People\"S Republic of", "code": "KP" },
    { "name": "Korea, Republic of", "code": "KR" },
    { "name": "Kuwait", "code": "KW" },
    { "name": "Kyrgyzstan", "code": "KG" },
    { "name": "Lao People\"S Democratic Republic", "code": "LA" },
    { "name": "Latvia", "code": "LV" },
    { "name": "Lebanon", "code": "LB" },
    { "name": "Lesotho", "code": "LS" },
    { "name": "Liberia", "code": "LR" },
    { "name": "Libyan Arab Jamahiriya", "code": "LY" },
    { "name": "Liechtenstein", "code": "LI" },
    { "name": "Lithuania", "code": "LT" },
    { "name": "Luxembourg", "code": "LU" },
    { "name": "Macao", "code": "MO" },
    { "name": "Macedonia, The Former Yugoslav Republic of", "code": "MK" },
    { "name": "Madagascar", "code": "MG" },
    { "name": "Malawi", "code": "MW" },
    { "name": "Malaysia", "code": "MY" },
    { "name": "Maldives", "code": "MV" },
    { "name": "Mali", "code": "ML" },
    { "name": "Malta", "code": "MT" },
    { "name": "Marshall Islands", "code": "MH" },
    { "name": "Martinique", "code": "MQ" },
    { "name": "Mauritania", "code": "MR" },
    { "name": "Mauritius", "code": "MU" },
    { "name": "Mayotte", "code": "YT" },
    { "name": "Mexico", "code": "MX" },
    { "name": "Micronesia, Federated States of", "code": "FM" },
    { "name": "Moldova, Republic of", "code": "MD" },
    { "name": "Monaco", "code": "MC" },
    { "name": "Mongolia", "code": "MN" },
    { "name": "Montserrat", "code": "MS" },
    { "name": "Morocco", "code": "MA" },
    { "name": "Mozambique", "code": "MZ" },
    { "name": "Myanmar", "code": "MM" },
    { "name": "Namibia", "code": "NA" },
    { "name": "Nauru", "code": "NR" },
    { "name": "Nepal", "code": "NP" },
    { "name": "Netherlands", "code": "NL" },
    { "name": "Netherlands Antilles", "code": "AN" },
    { "name": "New Caledonia", "code": "NC" },
    { "name": "New Zealand", "code": "NZ" },
    { "name": "Nicaragua", "code": "NI" },
    { "name": "Niger", "code": "NE" },
    { "name": "Nigeria", "code": "NG" },
    { "name": "Niue", "code": "NU" },
    { "name": "Norfolk Island", "code": "NF" },
    { "name": "Northern Mariana Islands", "code": "MP" },
    { "name": "Norway", "code": "NO" },
    { "name": "Oman", "code": "OM" },
    { "name": "Pakistan", "code": "PK" },
    { "name": "Palau", "code": "PW" },
    { "name": "Palestinian Territory, Occupied", "code": "PS" },
    { "name": "Panama", "code": "PA" },
    { "name": "Papua New Guinea", "code": "PG" },
    { "name": "Paraguay", "code": "PY" },
    { "name": "Peru", "code": "PE" },
    { "name": "Philippines", "code": "PH" },
    { "name": "Pitcairn", "code": "PN" },
    { "name": "Poland", "code": "PL" },
    { "name": "Portugal", "code": "PT" },
    { "name": "Puerto Rico", "code": "PR" },
    { "name": "Qatar", "code": "QA" },
    { "name": "Reunion", "code": "RE" },
    { "name": "Romania", "code": "RO" },
    { "name": "Russian Federation", "code": "RU" },
    { "name": "RWANDA", "code": "RW" },
    { "name": "Saint Helena", "code": "SH" },
    { "name": "Saint Kitts and Nevis", "code": "KN" },
    { "name": "Saint Lucia", "code": "LC" },
    { "name": "Saint Pierre and Miquelon", "code": "PM" },
    { "name": "Saint Vincent and the Grenadines", "code": "VC" },
    { "name": "Samoa", "code": "WS" },
    { "name": "San Marino", "code": "SM" },
    { "name": "Sao Tome and Principe", "code": "ST" },
    { "name": "Saudi Arabia", "code": "SA" },
    { "name": "Senegal", "code": "SN" },
    { "name": "Serbia and Montenegro", "code": "CS" },
    { "name": "Seychelles", "code": "SC" },
    { "name": "Sierra Leone", "code": "SL" },
    { "name": "Singapore", "code": "SG" },
    { "name": "Slovakia", "code": "SK" },
    { "name": "Slovenia", "code": "SI" },
    { "name": "Solomon Islands", "code": "SB" },
    { "name": "Somalia", "code": "SO" },
    { "name": "South Africa", "code": "ZA" },
    { "name": "South Georgia and the South Sandwich Islands", "code": "GS" },
    { "name": "Spain", "code": "ES" },
    { "name": "Sri Lanka", "code": "LK" },
    { "name": "Sudan", "code": "SD" },
    { "name": "Suriname", "code": "SR" },
    { "name": "Svalbard and Jan Mayen", "code": "SJ" },
    { "name": "Swaziland", "code": "SZ" },
    { "name": "Sweden", "code": "SE" },
    { "name": "Switzerland", "code": "CH" },
    { "name": "Syrian Arab Republic", "code": "SY" },
    { "name": "Taiwan", "code": "TW" },
    { "name": "Tajikistan", "code": "TJ" },
    { "name": "Tanzania, United Republic of", "code": "TZ" },
    { "name": "Thailand", "code": "TH" },
    { "name": "Timor-Leste", "code": "TL" },
    { "name": "Togo", "code": "TG" },
    { "name": "Tokelau", "code": "TK" },
    { "name": "Tonga", "code": "TO" },
    { "name": "Trinidad and Tobago", "code": "TT" },
    { "name": "Tunisia", "code": "TN" },
    { "name": "Turkey", "code": "TR" },
    { "name": "Turkmenistan", "code": "TM" },
    { "name": "Turks and Caicos Islands", "code": "TC" },
    { "name": "Tuvalu", "code": "TV" },
    { "name": "Uganda", "code": "UG" },
    { "name": "Ukraine", "code": "UA" },
    { "name": "United Arab Emirates", "code": "AE" },
    { "name": "United Kingdom", "code": "GB" },
    { "name": "United States", "code": "US" },
    { "name": "United States Minor Outlying Islands", "code": "UM" },
    { "name": "Uruguay", "code": "UY" },
    { "name": "Uzbekistan", "code": "UZ" },
    { "name": "Vanuatu", "code": "VU" },
    { "name": "Venezuela", "code": "VE" },
    { "name": "Viet Nam", "code": "VN" },
    { "name": "Virgin Islands, British", "code": "VG" },
    { "name": "Virgin Islands, U.S.", "code": "VI" },
    { "name": "Wallis and Futuna", "code": "WF" },
    { "name": "Western Sahara", "code": "EH" },
    { "name": "Yemen", "code": "YE" },
    { "name": "Zambia", "code": "ZM" },
    { "name": "Zimbabwe", "code": "ZW" }
];

function http(url, method, value = null) {
    return new Promise((resolve, reject) => {

        let xhr = new XMLHttpRequest();
        xhr.open(method, url, true);

        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onload = function () {
            if (this.status === 200 || this.status === 201) {
                resolve(this.response);
            } else {
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        }

        xhr.onerror = function () {
            reject(new Error("Network Error"));
        }

        xhr.send(value);
    });
}
async function importUsers(count = 1) {
    let response = await http("https://randomuser.me/api?results=" + count, 'GET');
    let users = getNewUsers(JSON.parse(response).results);
    return users;
}
async function addUsers(user) {
    let response = await http("http://localhost:3000/users", 'POST', JSON.stringify(user));
    console.log(response);
}

var users_all = [];
getUsers();
var statistic = new Chart(document.getElementById("stats"),
    {
        type: 'pie',
        options: {
            plugins: {
                legend: {
                    display: true
                },
                tooltip: {
                    enabled: true
                }
            }
        }
    });
var statFunc = getStatsByAge;
btnInit([getStatsByAge, getStatsByGender, getStatsByFavorite, getStatsByCourse, getStatsByCountry]);
let selects = document.querySelectorAll(".filter-input");
let region = selects[1];
let sortregion = getSorted(countries, "code");
for (let i = 0; i < sortregion.length; i++) {
    let country = new Option(sortregion[i].code, sortregion[i].name);
    region.options.add(country);
}

for (let s of selects) {
    s.onchange = renderUsers;
}
let search = document.getElementById("search-btn");
search.onclick = renderUsers;
let country_input = document.getElementsByName("country")[0];
sortregion = _.map(getSorted(countries, "name"), 'name');
for (let i = 0; i < sortregion.length; i++) {
    let country = new Option(sortregion[i]);
    country_input.options.add(country);
}
document.getElementById("form").addEventListener("submit", function (event) {
    event.preventDefault();
    addUser();
});
document.querySelectorAll(".new-users")[0].onclick = () => { getUsers(10) };